import React, { useContext } from "react";
import './RepDetails.scss';
import personContext from "../../hooks/personContext.tsx";

const RepDetails: React.FC = () => {
    const { person } = useContext(personContext);
    const placeHolder = {
        name: 'Name', 
        party: 'Party',
        state: 'State', 
        district: 'District',
        phone: 'Phone', 
        office: 'Office', 
        link: 'Link'
    }

    return (
        <div className="details-container">
            <h2>Info</h2>
            <div>{person?.name || placeHolder.name}</div>
            <div>{person?.party || placeHolder.party}</div>
            <div>{person?.state || placeHolder.state}</div>
            <div>{person?.district || placeHolder.district}</div>
            <div>{person?.phone || placeHolder.phone}</div>
            <div>{person?.office || placeHolder.office}</div>
            <div>{person?.link || placeHolder.link}</div>
        </div>
    );
};

export default RepDetails;