import React, { useContext, useEffect, useState } from 'react';
import './RepPicker.scss';
import Select from 'react-select';
import { REPRESENTATIVE, SENATE, SERVER_REP, SERVER_SEN, STATES } from '../../constants/constData.js';
import { ToggleButton, ToggleButtonGroup } from '@mui/material';
import { Person } from '../../models/Person.tsx';
import RepTable from '../RepTable/RepTable.tsx';
import personContext from '../../hooks/personContext.tsx';

const RepPicker: React.FC = () => {
    const [repType, setRepType] = useState<string>(REPRESENTATIVE);
    const [state, setState] = useState<string | null>(null);
    const [error, setError] = useState<any>(null);
    const [reps, setReps] = useState<Person[] | null>(null);
    const { person, setPerson } = useContext(personContext);

    function handleRepTypeChange(val) {
        setRepType(val);
        setReps(null);
        if(person) {
            setPerson(null);
        }
    }

    function handleStateChange(val) {
        setState(val);
        if(person) {
            setPerson(null);
        }
    }

    useEffect(() => {
        if(state) {
            let queryURL = repType === REPRESENTATIVE ? SERVER_REP : SERVER_SEN;
            queryURL += state;
            fetch(queryURL)
                .then((res) => res.json())
                .then((res) => {
                    if (res.success) {
                        let reps = [];
                        for(const item of res.results) {
                            reps.push(new Person(item));
                        }
                        setReps(res.results);
                    } else {
                        setError('Error on fetch');
                    }
                })
                .catch((error) => {
                    setError(error);
                })
        }
    }, [state]);

    return (
    <div className='picker-container'>
        <h3>Please select an option:</h3>
        <div className='row'>
            <ToggleButtonGroup 
                className='toggle'
                value={repType}
                color={'error'}
                onChange={(event, val) => handleRepTypeChange(val)}
                exclusive>
                <ToggleButton value={REPRESENTATIVE}>{REPRESENTATIVE}</ToggleButton>
                <ToggleButton value={SENATE}>{SENATE}</ToggleButton>
            </ToggleButtonGroup>
            {repType && 
            <Select
                className='select'
                options={STATES}
                onChange={(item) => handleStateChange(item.value)}/>}
            {error && <p>Error: {error}</p>}
        </div>
        {repType && reps && <RepTable data={reps}/>}
    </div>);
};

export default RepPicker;