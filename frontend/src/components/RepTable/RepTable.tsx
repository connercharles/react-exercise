import React, { useContext, useState } from "react";
import { Person } from "../../models/Person.tsx";
import personContext from "../../hooks/personContext.tsx";
import './RepTable.scss';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

interface props {
    data: Person[];
};

const RepTable: React.FC<props> = ({data}) => {
  const [selected, setSelected] = useState<string | null>(null);
  const { setPerson } = useContext(personContext);

  function handleClick(row) {
    setPerson(new Person(row));
    setSelected(row.name);
  }

  return (
    <div className="table-container">
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow className="header">
              <TableCell className="name">Name</TableCell>
              <TableCell className="party">Party</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row) => {
              const isSelected = row.name === selected;

              return (
                <TableRow 
                  hover 
                  key={row.name} 
                  selected={isSelected}
                  onClick={() => handleClick(row)}>
                  <TableCell className="name">{row.name}</TableCell>
                  <TableCell className="party">{row.party}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default RepTable;
