
export class Person {
    public name: string;
    public party: string;
    public state: string;
    public district: string;
    public phone: string;
    public office: string;
    public link: string;

    constructor(data: Object) {
        this.name = data.name;
        this.party = data.party;
        this.state = data.state;
        this.district = data.district;
        this.phone = data.phone;
        this.office = data.office;
        this.link = data.link;
    }
    
};