import React, { useState } from 'react';
import './App.scss';
import RepPicker from './components/RepPicker/RepPicker.tsx';
import RepDetails from './components/RepDetails/RepDetails.tsx';
import { Person } from './models/Person.tsx';
import personContext from './hooks/personContext.tsx';

const App: React.FC = () => {
  const [person, setPerson] = useState<Person | null>(null);

  return (
    <personContext.Provider value={{ person, setPerson }}>
      <div className="app">
        <header className="header">
          <h1>Who's My Representative?</h1>
        </header>
        <div className='body'>
          <RepPicker/>
          <RepDetails/>
        </div>
      </div>
    </personContext.Provider>
  );
}

export default App;
